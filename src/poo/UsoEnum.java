/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Scanner;

/**
 *
 * @author cuahutli
 */
public class UsoEnum {
//    enum Talla {MINI, MEDIANO, GRANDE, MUY_GRANDE};
    
    enum Talla{        
        MINI("S"), MEDIANO("M"), GRANDE("L"), MUY_GRANDE("XL");
        
        private String abreviatura;
        
        private Talla(String abreviatura){
            this.abreviatura = abreviatura;
        }
        
        private String getAbreviatura(){
            return abreviatura;
        }
        
        
    }
    
    public static void main(String[] args) {
      
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe una talla: MINI, MEDIANO, GRANDE O MUY GRANDE");
        String entradaDatos = sc.next().toUpperCase();
        Talla t = Enum.valueOf(Talla.class, entradaDatos);
        System.out.println("Talla = " + t + "Abrv= " + t.getAbreviatura());
       
    }
    
}
