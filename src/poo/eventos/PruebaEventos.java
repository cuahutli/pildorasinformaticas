/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author cuahutli
 */
public class PruebaEventos {
    
    public static void main (String[] args){
        MarcoBotones mimarco = new MarcoBotones();
        mimarco.setVisible(true);
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
}

class MarcoBotones extends JFrame{
    public MarcoBotones(){
        setTitle("Botones y Eventos");
        setBounds(700,300,500,300);
        LaminaBotones milamina = new LaminaBotones();
        add(milamina);
    }
}


class LaminaBotones extends JPanel{
    JButton botonAzul = new JButton("Azul");
    JButton botonRojo = new JButton("Rojo");
    JButton botonAmarillo = new JButton("Amarillo");
    
    
    public LaminaBotones(){
        add(botonAzul);
        add(botonRojo);
        add(botonAmarillo);
        
        ColorFondo Amarillo = new ColorFondo(Color.YELLOW);
        ColorFondo Azul = new ColorFondo(Color.BLUE);
        ColorFondo Rojo = new ColorFondo(Color.RED);
        
        //addActionListener es como decirle "al hacer click"
        botonAzul.addActionListener(Azul);
        botonRojo.addActionListener(Rojo);
        botonAmarillo.addActionListener(Amarillo);
    }
    
    private class ColorFondo implements ActionListener{
        private Color colorDeFondo;

        public ColorFondo(Color c){
            colorDeFondo = c;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            setBackground(colorDeFondo);
        }

    }

}