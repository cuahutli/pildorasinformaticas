
package poo.eventos;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author cuahutli
 */
public class MultiplesOyentes {
    public static void main(String[] args) {
        MarcoPrincipal marco = new MarcoPrincipal();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        marco.setVisible(true);
    }

}

class MarcoPrincipal extends JFrame{
    public MarcoPrincipal(){
        setBounds(300,300,500,350);
        setTitle("Ejemplo Multiples Oyentes");
        LaminaPrincipal lamina = new LaminaPrincipal();
        add(lamina);
    }
}


class LaminaPrincipal extends JPanel{
    public LaminaPrincipal(){
        JButton btnNuevo = new JButton("Nuevo");
//        btnNuevo.addActionListener(new ActionListener(){
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                MarcoEmergente marcoE = new MarcoEmergente();
//                marcoE.setVisible(true);
//            }
//        
//        });

        ActionListener fuente = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                MarcoEmergente marcoE = new MarcoEmergente(btnCerrar);
                marcoE.setVisible(true);
            }
        
        };
        btnNuevo.addActionListener(fuente);
        btnCerrar = new JButton("Cerrar Todo");       
        add(btnNuevo);
        add(btnCerrar);
    }
    
    JButton btnCerrar;
}

class MarcoEmergente extends JFrame{
    
    public MarcoEmergente(JButton btnCerrarDesdePrincipal){
        contador ++;
        setTitle("Ventana " + contador);
        setBounds(40*contador, 40*contador, 400, 350);
        btnCerrarDesdePrincipal.addActionListener(new CierraTodos());
    }
    
    private static int contador = 0;
    
    
    private class CierraTodos implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            dispose();
        }
    }
}

