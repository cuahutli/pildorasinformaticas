package poo.eventos;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author cuahutli
 */
public class EventosRaton {

    public static void main(String[] args) {
        MarcoRaton marco = new MarcoRaton();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

class MarcoRaton extends JFrame {

    public MarcoRaton() {
        setVisible(true);
        setBounds(700, 300, 600, 450);
        EventosDeRaton evr = new EventosDeRaton();
        JButton boton = new JButton("Probando MouseEvent");
        boton.setBounds(300, 300, 50, 30);
        boton.addMouseListener(evr);
        add(boton);
    }
}

class EventosDeRaton implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("has hecho click   X: " + e.getX() + " Y: " + e.getY());
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("acabas de presionar");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("acabas de levantar");
    }

    @Override
    public void mouseEntered(MouseEvent e){        
        System.out.println("has hecho click   X: " + e.getX() + " Y: " + e.getY());
    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
