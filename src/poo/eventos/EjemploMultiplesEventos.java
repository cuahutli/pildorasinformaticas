
package poo.eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

/**
 *
 * @author cuahutli
 */
public class EjemploMultiplesEventos {
    public static void main(String[] args) {
        MarcoAccion marco = new MarcoAccion();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}


class MarcoAccion extends JFrame{
    public MarcoAccion(){
        setTitle("Multiples acciones");
        setBounds(600,350,600,300);
        PanelAccion panel = new PanelAccion();
        add(panel);
        setVisible(true);
    }
}

class PanelAccion extends JPanel{
    public PanelAccion(){
        //instanciamos la clase AcciónColor
        AccionColor acy = new AccionColor("Amarillo", new ImageIcon("src/img/amarillo.png"), Color.YELLOW );
        AccionColor acb = new AccionColor("Azul", new ImageIcon("src/img/azul.png"), Color.BLUE );
        AccionColor acr = new AccionColor("Rojo", new ImageIcon("src/img/rojo.png"), Color.RED );
        
        JButton boton1 = new JButton(acy);
        JButton boton2 = new JButton(acr);
        JButton boton3 = new JButton(acb);
        
        
        add(boton1);
        add(boton2);
        add(boton3);
        
        
        InputMap mapaEntrada = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap mapaAccion = getActionMap();
        //KeyStroke  teclaAmarillo = KeyStroke.getKeyStroke("ctrl A");
//        mapaEntrada.put(teclaAmarillo, "fondoAmarillo");
        mapaEntrada.put(KeyStroke.getKeyStroke("ctrl A"), "fondoAmarillo");
        mapaEntrada.put(KeyStroke.getKeyStroke("ctrl T"), "fondoAmarillo");
        mapaAccion.put("fondoAmarillo", acy);
        
        
        mapaEntrada.put(KeyStroke.getKeyStroke("ctrl B"), "fondoAzul");
        mapaAccion.put("fondoAzul", acb);
        
        mapaEntrada.put(KeyStroke.getKeyStroke("ctrl R"), "fondoRojo");
        mapaAccion.put("fondoRojo", acr);
    }
    
    
    private class AccionColor extends AbstractAction{
    
        public AccionColor(String nombre, Icon icono, Color colorBoton){
            putValue(Action.NAME, nombre);
            putValue(Action.SMALL_ICON, icono);
            putValue(Action.SHORT_DESCRIPTION, "Poner la lámina de color " + nombre);
            putValue("colorDeFondo", colorBoton);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Color c =  (Color) getValue("colorDeFondo");
            System.out.println("Nombre: " + getValue("Name") +  " Descripción: " + getValue("ShortDescription"));
            setBackground(c);
        }

    }
}

//class AccionColor extends AbstractAction{
//    
//    public AccionColor(String nombre, Icon icono, Color colorBoton){
//        putValue(Action.NAME, nombre);
//        putValue(Action.SMALL_ICON, icono);
//        putValue(Action.SHORT_DESCRIPTION, "Poner la lámina de color " + nombre);
//        putValue("colorDeFondo", colorBoton);
//    }
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        Color c =  (Color) getValue("colorDeFondo");
//        setBackground(c);
//    }
//    
//}