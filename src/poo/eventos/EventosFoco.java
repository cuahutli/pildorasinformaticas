
package poo.eventos;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author cuahutli
 */
public class EventosFoco {
    public static void main(String[] args) {
        MarcoFoco marco = new MarcoFoco(){
            @Override
            public void nuevaFuncion(){System.out.println("overraideamos perrón");}
        };
        marco.nuevaFuncion();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
        
}


class MarcoFoco extends JFrame{
    
    public MarcoFoco(){
        setVisible(true);
        setBounds(300,300,600,450);
        add(new LaminaFoco());
    }
    
    public void nuevaFuncion(){
        System.out.println("imprimimos desde MarcoFoco");
    }
}

class LaminaFoco extends JPanel{
    
    JTextField cuadro1;
    JTextField cuadro2;
    
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        setLayout(null);
        CambiaColor cc = new CambiaColor();
        LanzaFocos lf = new LanzaFocos();
        cuadro1 = new JTextField();
        cuadro2 = new JTextField();
        cuadro1.setBounds(120, 10, 150, 20);
        cuadro2.setBounds(120, 50, 150, 20);
        cuadro1.addMouseListener(cc);
        cuadro1.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Presionado desde dentro");
            }
        });
        cuadro2.addMouseListener(cc);
        cuadro1.addFocusListener(lf);
        cuadro2.addFocusListener(lf);
        add(cuadro1);
        add(cuadro2);
        
    }
}

class CambiaColor extends MouseAdapter{
    public void mouseEntered(MouseEvent e){
        Component componente = (Component) e.getSource();
        componente.setBackground(Color.LIGHT_GRAY);
    }
    
    public void mouseExited(MouseEvent e){
        Component componente = (Component) e.getSource();
        componente.setBackground(Color.WHITE);
    }
}

class LanzaFocos implements FocusListener{

    @Override
    public void focusGained(FocusEvent e) {
        System.out.println("Ganamos el foco");
    }

    @Override
    public void focusLost(FocusEvent e) {
        System.out.println("Perdimos el foco");
    }

}