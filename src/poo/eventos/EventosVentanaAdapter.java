
package poo.eventos;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;

/**
 *
 * @author cuahutli
 */
public class EventosVentanaAdapter {
    public static void main (String[] args){
        MarcoVentanaA mimarco = new MarcoVentanaA();
        MarcoVentanaA mimarco2 = new MarcoVentanaA();
        mimarco.setTitle("Marco 1");
        mimarco.setBounds(300,300,500,350);
        mimarco2.setTitle("Ventana 2");
        mimarco2.setBounds(800,300,150,150);
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mimarco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            
    }

}

class MarcoVentanaA extends JFrame{
    public MarcoVentanaA(){
        setVisible(true);
//        MVentanaA oyenteVentana = new MVentanaA();
//        addWindowListener(oyenteVentana);
          addWindowListener(new MVentanaA());
    }
}


class MVentanaA extends WindowAdapter{

    public void windowIconified(WindowEvent e) {
        System.out.println("Hemos minimizado la ventana");
    }

}