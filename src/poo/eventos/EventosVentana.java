
package poo.eventos;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;

/**
 *
 * @author cuahutli
 */
public class EventosVentana {
    
    public static void main (String[] args){
        MarcoVentana mimarco = new MarcoVentana();
        MarcoVentana mimarco2 = new MarcoVentana();
        mimarco.setTitle("Marco 1");
        mimarco.setBounds(300,300,500,350);
        mimarco2.setTitle("Ventana 2");
        mimarco2.setBounds(800,300,150,150);
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mimarco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            
    }

}


class MarcoVentana extends JFrame{
    public MarcoVentana(){
        setVisible(true);
        MVentana oyenteVentana = new MVentana();
        addWindowListener(oyenteVentana);
    }
}


class MVentana implements WindowListener{

    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Ventana Abierta");
    }

    @Override
    public void windowClosing(WindowEvent e) {  
        System.out.println("Estamos cerrando la ventana");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("La ventana ha sido cerrada");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        System.out.println("Hemos minimizado la ventana");
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        System.out.println("Estamos maximizando la ventana minimizada");
    }

    @Override
    public void windowActivated(WindowEvent e) {
        System.out.println("Ventana Activada");
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        System.out.println("Ventana desactivada");
    }

}