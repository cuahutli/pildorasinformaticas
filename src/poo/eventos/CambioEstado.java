
package poo.eventos;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import javax.swing.JFrame;

/**
 *
 * @author cuahutli
 */
public class CambioEstado {
    public static void main (String[] args){
        MarcoEstado mimarco = new MarcoEstado();
        mimarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}

class MarcoEstado extends JFrame{
    public MarcoEstado(){
        setBounds(300,300,500,350);
        setVisible(true);
        this.addWindowStateListener(new CambiaEstado());
        
    }
}

class CambiaEstado implements WindowStateListener{

    @Override
    public void windowStateChanged(WindowEvent e) {
//        System.out.println("el estado ha cambiado." + e.getSource().getClass());
        if (e.getNewState() == Frame.MAXIMIZED_BOTH){
            System.out.println("La ventana ha cambiado de estado a pantalla completa");
        }
        
        if (e.getNewState() == Frame.ICONIFIED){
            System.out.println("La ventana ha cambiado de estado a minimizada");
        }
    }
    
}

