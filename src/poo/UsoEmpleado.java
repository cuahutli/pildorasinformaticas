/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author cuahutli
 */
public class UsoEmpleado {

    public static void main(String[] args) {
        Jefatura jefeSistemas = new Jefatura("Cuahutli", 12000, 2010, 11, 1);
        jefeSistemas.estableceIncentivo(2000);

        Empleado[] empleados = new Empleado[6];
        empleados[0] = new Empleado("Cecy", 7000, 2014, 12, 12);
        empleados[1] = new Empleado("Yudith", 8000, 2009, 11, 1);
        empleados[2] = new Empleado("Oscar", 7500, 2012, 11, 1);
        empleados[3] = new Empleado("Cesar", 7000, 2014, 12, 12);
        empleados[4] = jefeSistemas; //Polimorfismo en acción, Principio de sustitución es un
        empleados[5] = new Jefatura("Jhony", 12000, 2009, 06, 01);

        Jefatura jefeContabilidad = (Jefatura) empleados[5];
        jefeContabilidad.estableceIncentivo(2000);
        
        Empleado directorComercial  = new Jefatura("Enrique", 12000,2010,01,01);
        
        Comparable ejemplo = new Empleado("Yudi", 7000, 2010,01,01);
        
        System.out.println(jefeSistemas.tomarDeciciones("Dar más días de vacaiones"));
        
        System.out.println("El Jefe " + jefeContabilidad.getNombre() + " tiene de bonus "  + jefeContabilidad.estableceBonus(1000));
        System.out.println("El Jefe " + jefeSistemas.getNombre() + " tiene de bonus "  + jefeSistemas.estableceBonus(2000));
        System.out.println("El empleado" + empleados[3].getNombre() + " tiene de bonus" + empleados[3].estableceBonus(1000));
           
        for (Empleado e : empleados) {
            e.subeSueldo(10);
        }
        
        // el tipo a comparar debe implementar la interfaz comparable.
        // para usar esto, la clase Empleado tiene que implementar la interface comparable
        Arrays.sort(empleados);
        
        for (Empleado e : empleados) {
            System.out.println("==== Datos Empleado ===");
            System.out.println("Nombre: " + e.getNombre()
                    + "\nSueldo: " + e.getSueldo()
                    + "\nFecha Alta: " + e.getAltaContrato() + "\n");

        }
        
        

    }

}

class Empleado implements Comparable, Trabajadores{

    private String nombre;
    private double sueldo;
    private Date altaContrato;
    private int id;
    private static int idSiguiente;

    public Empleado(String nombre, double sueldo, int anhio, int mes, int dia) {
        this.nombre = nombre;
        this.sueldo = sueldo;
        GregorianCalendar cal = new GregorianCalendar(anhio, mes - 1, dia);
        this.altaContrato = cal.getTime();
        ++idSiguiente;
        id = idSiguiente;
    }

    public Empleado(String nombre) {
        this(nombre, 30000, 2000, 1, 1);
    }

    public String getNombre() {
        return nombre + " id = " + id;
    }

    public double getSueldo() {
        return sueldo;
    }

    public Date getAltaContrato() {
        return altaContrato;
    }

    public void subeSueldo(double porcentaje) {
        double aumento = sueldo * porcentaje / 100;
        sueldo += aumento;
    }
    
    @Override
    public int compareTo(Object miObjeto){
       //Cast(refundición empleado) 
       Empleado emp = (Empleado) miObjeto;
       
       if (this.sueldo < emp.sueldo){
           return -1;
       }else if(this.sueldo > emp.sueldo){
           return 1;
       }else{
           return 0;
       }
       
    }

    @Override
    public double estableceBonus(double gratificacion) {
        return Trabajadores.bonusBase + gratificacion;
    }
}

class Jefatura extends Empleado implements Jefes {

    private double incentivo;

    public Jefatura(String nombre, double sueldo, int anhio, int mes, int dia) {
        super(nombre, sueldo, anhio, mes, dia);

    }

    public void estableceIncentivo(double incentivo) {
        this.incentivo = incentivo;
    }

    @Override
    public double getSueldo() {
        double sueldoJefe = super.getSueldo();
        return sueldoJefe + incentivo;
    }

    @Override
    public String tomarDeciciones(String decision) {
        return "Un miembro de la dirección ha tomado la decisón de: " + decision;
    }
    
    @Override
    public double estableceBonus(double gratificacion){
        double prima = 2000;
        return Jefes.bonusBase + gratificacion + prima;
    }

}
