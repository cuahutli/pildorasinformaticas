/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.Timer;



/**
 *
 * @author cuahutli
 */
public class PruebaTemporizador {
    
    
    
    public static void main(String[] args) {
        Reloj reloj = new Reloj();
        reloj.enMarcha(3000, true);
        JOptionPane.showMessageDialog(null, "Pulsa Aceptar para terminar");
        System.exit(0);
       
    }
    
}


class Reloj{
        
    public void enMarcha(int intervalo, boolean sonido){
        
        class DameLaHora2 implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                Date ahora = new Date();
                System.out.println("La hora es: " + ahora + " cada 3 segundos");

                if(sonido){
                    Toolkit.getDefaultToolkit().beep();
                }

            }

        }
        
        ActionListener oyente = new DameLaHora2();
        Timer timer = new Timer(intervalo, oyente);
        timer.start();
    }
    
}