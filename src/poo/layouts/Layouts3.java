package poo.layouts;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author cuahutli
 */
public class Layouts3 {

    public static void main(String[] args) {
        MarcoLayout3 marco = new MarcoLayout3();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        marco.setVisible(true);
    }
}

class MarcoLayout3 extends JFrame {

    public MarcoLayout3() {
        setTitle("Prueba de Layouts");
        setBounds(600, 350, 600, 300);
        PanelLayout3 lamina = new PanelLayout3();
        add(lamina);
    }
}

class PanelLayout3 extends JPanel {

    public PanelLayout3() {
        setLayout(new BorderLayout(5, 5));
        JButton boton = new JButton("0");
        boton.setHorizontalAlignment(SwingConstants.RIGHT);
        boton.setEnabled(false);
        add(boton, BorderLayout.NORTH);
        PanelLayout33 lamina2 = new PanelLayout33();
        add(lamina2, BorderLayout.CENTER);

    }
}

class PanelLayout33 extends JPanel {

    public PanelLayout33() {
        setLayout(new GridLayout(4, 4));

        ponerBoton("7");
        ponerBoton("8");
        ponerBoton("9");
        ponerBoton("/");
        ponerBoton("4");
        ponerBoton("5");
        ponerBoton("6");
        ponerBoton("*");
        ponerBoton("1");
        ponerBoton("2");
        ponerBoton("3");
        ponerBoton("-");
        ponerBoton("0");
        ponerBoton(".");
        ponerBoton("=");
        ponerBoton("+");
    }

    private void ponerBoton(String rotulo) {
        JButton boton = new JButton(rotulo);
        add(boton);
    }
}
