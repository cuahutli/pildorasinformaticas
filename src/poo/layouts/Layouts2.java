
package poo.layouts;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author cuahutli
 */
public class Layouts2 {
    public static void main(String[] args) {
        MarcoLayout2 marco = new MarcoLayout2();
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        marco.setVisible(true);
    }
}

class MarcoLayout2 extends JFrame{
    public MarcoLayout2(){
        setTitle("Prueba de Layouts");
        setBounds(600,350,600,300);
        setLayout(new BorderLayout());
        PanelLayout2 lamina = new PanelLayout2();
        PanelLayout22 lamina2 = new PanelLayout22();
        add(lamina,BorderLayout.NORTH);
        add(lamina2,BorderLayout.SOUTH);
    }
}

class PanelLayout2 extends JPanel{
    public PanelLayout2(){
//        setLayout(new BorderLayout());
//        setLayout(new BorderLayout(10,10));
        setLayout(new FlowLayout(FlowLayout.LEFT));
        add(new JButton("Amarillo"));
        add(new JButton("Rojo"));
        
    
    }
}

class PanelLayout22 extends JPanel{
    public PanelLayout22(){
        setLayout(new BorderLayout());
        add(new JButton("Azul"), BorderLayout.WEST);//oeste
        add(new JButton("Verde"), BorderLayout.EAST);//este
        add(new JButton("NEGRO"), BorderLayout.CENTER);//center
    }
}